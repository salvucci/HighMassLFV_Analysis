/* ROOT includes */
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TFile.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TColor.h>
#include <TLatex.h>

/* C++ includes */
#include "iostream"

void LimitInputs(int LFVtype, bool useBjet,
		 std::string process, bool useSmooth);
