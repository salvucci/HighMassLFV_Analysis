#include <TROOT.h>
#include <TH1F.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TF1.h>
#include <TFile.h>
#include <TApplication.h>
#include <TRint.h>
/* C++ includes */
#include <iomanip>
#include <iostream>
/* Other includes */
#include "Utils.h"

void SigDistro(int LFVtype, bool useBjet, bool normalize);
void SetHistoStyle(TH1F* hV, int i, double Xmin, bool normalize);

bool m_debug;
