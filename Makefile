CXX = clang++
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
   CXX = g++
endif

CXXFLAGS = -O -Wall -fPIC

## Libs
ROOTCXXFLAGS = $(shell root-config --cflags)
ROOTLDFLAGS  = $(shell root-config --ldflags)
ROOTLIBS     = $(shell root-config --libs)
ROOTGLIBS    = $(shell root-config --glibs)

LIBS = $(ROOTLIBS)

CXXFLAGS += $(ROOTCXXFLAGS)

#RunOnMiniTree
MI_OBJ  = RunOnMiniTree.o
MI_H    = RunOnMiniTree.h
MI_DIC  = Dic_RunOnMiniTree.cxx
MI_LDEF = LinkDef_RunOnMiniTree.h
MI_LIB  = libRunOnMiniTree.so

#AdjustFakeRate
AD_OBJ  = AdjustFakeRate.o

#TopExtra
TE_OBJ = TopExtra.o

#TopScale
TS_OBJ = TopScale.o

#CheckTopFit
TF_OBJ = CheckTopFits.o

#MakeQCD
QC_OBJ = MakeQCD.o

#AddContribution
CT_OBJ = AddContributions.o

#MergeContributions
MC_OBJ = MergeContributions.o

#SysPercentVariation
SV_OBJ = SysPercentVariations.o

#CreateSysBands
SB_OBJ = CreateSysBands.o

#LimitInputs
LI_OBJ = LimitInputs.o

#MakeSystPlots
SP_OBJ = MakeSystPlots.o

#DataPeriodPlot
DP_OBJ = DataPeriodPlot.o

#DrawLFVxsec
DX_OBJ = DrawLFVxsec.o

#MakeDataMCplots
MP_OBJ = MakeDataMCplots.o

#GetSysValues
GS_OBJ = GetSysValues.o

#DrawQcdPlots
DQ_OBJ = DrawQcdPlots.o

#kMJ_Fits
KF_OBJ = kMJ_Fits.o

#DerivePtRw
RW_OBJ = DerivePtRw.o

#DrawWjetsPlots
DW_OBJ = DrawWjetsPlots.o

#DrawTopTTV
DT_OBJ = DrawTopTTV.o

#FakeBkgSmooth
SM_OBJ = FakeBkgSmooth.o

#SigDistro
SD_OBJ = SigDistro.o

##Build
all: dicmi mi libmi ad te ts tf qcd ct mc sv sb li sp dp dx mp gs dq kf rw dw dt sm sd

#RunOnMiniTree
dicmi: $(MI_H)
	@echo ""
	@echo "Building RunOnMiniTree dictionary ... "
	@rootcint -f $(MI_DIC) -c $(CXXFLAGSPKG) -p $(MI_H) $(MI_LDEF)

mi:	$(MI_OBJ)
	@echo ""
	@echo "Compiling objects ($(MI_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(MI_OBJ) $(ROOTLDFLAGS) -o RunOnMiniTree -g $(LIBS) -lm -lEG

libmi: $(MI_OBJ)
	@echo""
	@echo "Linking library ($(MI_LIB)) ... "
	@$(CXX) $(CXXFLAGS) -shared $(LIBS) $(MI_OBJ) -o $(MI_LIB)

#AdjustFakeRate
ad:	$(AD_OBJ)
	@echo ""
	@echo "Compiling objects ($(AD_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(AD_OBJ) $(ROOTLDFLAGS) -o AdjustFakeRate -g $(LIBS) -lm -lEG

#TopExtra
te:     $(TE_OBJ)
	@echo ""
	@echo "Compiling objects ($(TE_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(TE_OBJ) $(ROOTLDFLAGS) -o TopExtra -g $(LIBS) -lm -lEG

#TopExtra
ts:     $(TS_OBJ)
	@echo ""
	@echo "Compiling objects ($(TS_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(TS_OBJ) $(ROOTLDFLAGS) -o TopScale -g $(LIBS) -lm -lEG

#CheckTopFits
tf:     $(TF_OBJ)
	@echo ""
	@echo "Compiling objects ($(TF_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(TF_OBJ) $(ROOTLDFLAGS) -o CheckTopFits -g $(LIBS) -lm -lEG

#MakeQCD
qcd:	$(QC_OBJ)
	@echo ""
	@echo "Compiling objects ($(QC_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(QC_OBJ) $(ROOTLDFLAGS) -o MakeQCD -g $(LIBS) -lm -lEG

#AddContribution
ct:     $(CT_OBJ)
	@echo ""
	@echo "Compiling objects ($(CT_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(CT_OBJ) $(ROOTLDFLAGS) -o AddContributions -g $(LIBS) -lm -lEG

#MergeContributions
mc:	$(MC_OBJ)
	@echo ""
	@echo "Compiling objects ($(MC_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(MC_OBJ) $(ROOTLDFLAGS) -o MergeContributions -g $(LIBS) -lm -lEG

#SysPercentVariations
sv:	$(SV_OBJ)
	@echo ""
	@echo "Compiling objects ($(SV_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(SV_OBJ) $(ROOTLDFLAGS) -o SysPercentVariations -g $(LIBS) -lm -lEG

#CreateSysBands
sb:	$(SB_OBJ)
	@echo ""
	@echo "Compiling objects ($(SB_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(SB_OBJ) $(ROOTLDFLAGS) -o CreateSysBands -g $(LIBS) -lm -lEG

#LimitInputs
li:	$(LI_OBJ)
	@echo ""
	@echo "Compiling objects ($(LI_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(LI_OBJ) $(ROOTLDFLAGS) -o LimitInputs -g $(LIBS) -lm -lEG

#MakeSystPlots
sp:	$(SP_OBJ)
	@echo ""
	@echo "Compiling objects ($(SP_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(SP_OBJ) $(ROOTLDFLAGS) -o MakeSystPlots -g $(LIBS) -lm -lEG

#DataPeriodPlot
dp:	$(DP_OBJ)
	@echo ""
	@echo "Compiling objects ($(DP_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(DP_OBJ) $(ROOTLDFLAGS) -o DataPeriodPlot -g $(LIBS) -lm -lEG

#DrawLFVxsec
dx:	$(DX_OBJ)
	@echo ""
	@echo "Compiling objects ($(DX_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(DX_OBJ) $(ROOTLDFLAGS) -o DrawLFVxsec -g $(LIBS) -lm -lEG

#MakeDataMCplots
mp:	$(MP_OBJ)
	@echo ""
	@echo "Compiling objects ($(MP_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(MP_OBJ) $(ROOTLDFLAGS) -o MakeDataMCplots -g $(LIBS) -lm -lEG

#GetSysValues
gs: 	$(GS_OBJ)
	@echo ""
	@echo "Compiling objects ($(GS_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(GS_OBJ) $(ROOTLDFLAGS) -o GetSysValues -g $(LIBS) -lm -lEG

#DrawQcdPlots
dq: 	$(DQ_OBJ)
	@echo ""
	@echo "Compiling objects ($(DQ_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(DQ_OBJ) $(ROOTLDFLAGS) -o DrawQcdPlots -g $(LIBS) -lm -lEG

#kMJ_Fits
kf: 	$(KF_OBJ)
	@echo ""
	@echo "Compiling objects ($(KF_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(KF_OBJ) $(ROOTLDFLAGS) -o kMJ_Fits -g $(LIBS) -lm -lEG

#DerivePtRw
rw: 	$(RW_OBJ)
	@echo ""
	@echo "Compiling objects ($(RW_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(RW_OBJ) $(ROOTLDFLAGS) -o DerivePtRw -g $(LIBS) -lm -lEG

#DrawWjetsPlots
dw: 	$(DW_OBJ)
	@echo ""
	@echo "Compiling objects ($(DW_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(DW_OBJ) $(ROOTLDFLAGS) -o DrawWjetsPlots -g $(LIBS) -lm -lEG

#DrawTopTTV
dt: 	$(DT_OBJ)
	@echo ""
	@echo "Compiling objects ($(DT_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(DT_OBJ) $(ROOTLDFLAGS) -o DrawTopTTV -g $(LIBS) -lm -lEG

#FakeBkgSmooth
sm: 	$(SM_OBJ)
	@echo ""
	@echo "Compiling objects ($(SM_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(SM_OBJ) $(ROOTLDFLAGS) -o FakeBkgSmooth -g $(LIBS) -lm -lEG

#SigDistro
sd: 	$(SD_OBJ)
	@echo ""
	@echo "Compiling objects ($(SD_OBJ)) ... "
	@$(CXX) -fPIC -D_REENTRANT $(SD_OBJ) $(ROOTLDFLAGS) -o SigDistro -g $(LIBS) -lm -lEG

## Clean
clean:
	@echo "Cleaning path ... "
	rm -rf *.o
	rm -rf RunOnMiniTree
	rm -rf AdjustFakeRate
	rm -rf TopExtra
	rm -rf TopScale
	rm -rf CheckTopFits
	rm -rf MakeQCD
	rm -rf AddContributions
	rm -rf MergeContributions
	rm -rf SysPercentVariations
	rm -rf CreateSysBands
	rm -rf LimitInputs
	rm -rf MakeSystPlots
	rm -rf DataPeriodPlot
	rm -rf DrawLFVxsec
	rm -rf MakeDataMCplots
	rm -rf GetSysValues
	rm -rf DrawQcdPlots
	rm -rf kMJ_Fits
	rm -rf DerivePtRw
	rm -rf DrawWjetsPlots
	rm -rf DrawTopTTV
	rm -rf FakeBkgSmooth
	rm -rf SigDistro
	rm -rf *~
	rm -rf *.so
	rm -rf *.pcm
	rm -rf Dic_RunOnMiniTree*
	rm -rf *.d
